import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Post } from 'src/app/shared/interfaces';
import { PostService } from 'src/app/shared/post.service';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
})
export class DashboardPageComponent implements OnInit, OnDestroy {
  posts: Post[] = [];
  pSub: Subscription = new Subscription();
  dSub: Subscription = new Subscription();
  searchStr: string = '';

  constructor(
    private postService: PostService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.pSub = this.postService.getAll().subscribe((posts) => {
      this.posts = posts;
      console.log(posts);
    });
    console.log(this.pSub, 'psub');
  }

  ngOnDestroy(): void {
    // get posts
    if (this.pSub) {
      this.pSub.unsubscribe();
    }

    // delete post
    if (this.dSub) {
      this.dSub.unsubscribe();
    }
  }
  remove(id: string | undefined): void {
    this.dSub = this.postService.removePost(id).subscribe(() => {
      this.posts = this.posts?.filter((post) => post?.id !== id);
      this.alertService.warning('Post was removed');
    });
  }
}
