import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { QuillModule } from 'ngx-quill';
import { AuthService } from '../admin/shared/services/auth.service';
import { PostService } from './post.service';

@NgModule({
  imports: [HttpClientModule, QuillModule.forRoot()],
  exports: [HttpClientModule, QuillModule],
  providers: [AuthService, PostService],
})
export class SharedModule {}
