import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { FbCreateResponse, Post } from './interfaces';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs';

@Injectable()
export class PostService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<Post[]> {
    return this.http.get(`${environment.fbDbUrl}/posts.json`).pipe(
      map((res: { [key: string]: any }) => {
        return Object.keys(res)?.map((key: any) => ({
          ...res[key],
          id: key,
          date: new Date(res[key].date),
        }));
      })
    );
  }

  getPostById(id: string): Observable<Post> {
    return this.http.get<Post>(`${environment.fbDbUrl}/posts/${id}.json`).pipe(
      map((post: Post) => {
        const newPost = {
          ...post,
          id,
          date: new Date(post.date),
        };
        return newPost;
      })
    );
  }

  create(post: Post): Observable<Post> {
    return this.http.post(`${environment.fbDbUrl}/posts.json`, post).pipe(
      map((res: FbCreateResponse) => {
        const newPost = {
          ...post,
          id: res.name,
          date: new Date(post.date),
        };
        return newPost;
      })
    );
  }

  update(post: Post): Observable<Post> {
    console.log(post, 'post');

    return this.http.patch<Post>(
      `${environment.fbDbUrl}/posts/${post?.id}.json`,
      post
    );
  }

  removePost(id: string | undefined): Observable<void> {
    return this.http.delete<void>(`${environment.fbDbUrl}/posts/${id}.json`);
  }
}
